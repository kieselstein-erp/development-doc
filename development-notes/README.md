# Development notes

For the moment, this contains more-or-less unsorted notes concerning development. However, soon this should be sorted out into a properly structured documentation.

## First Roadmap proposal (KF)
1. merge repos into 1 (e.g. kieselstein-reactor) REsoning:
   * makes it easier as there are cross-deps; Furthermore, anyhow always client and server 
   * makes it easier to move files between projects while keeping the history (we might have to move quite a few...;-) 
2. Put a build tool in place: gradle or maven?
   * I (KF) would try with gradle as more flexible and more concise. Depends a bit where wildfly is easier to handle.
   * Potentially first simply compile, later compose wildefly war ..
3. Put [flyway](https://flywaydb.org/) in place (alternative: [liquibase](https://www.liquibase.org/) - might be an overkill). 
   * bootstrap with actual db schema -> From then on migrations
4. compose docker image (postgres, wildfly, app)
5. Some cleanup (source folders....)
6. put some rudimentary tests in place

## External deps

* Wildfly 12
* Java EE 8 (Zulu)
