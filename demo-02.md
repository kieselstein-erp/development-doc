# Sprint 02

## Goal: "Anwender können Kieselstein & Terminal installieren und Testen"

---

Still to do before demo:

* Update readme for terminal installation
* KF: Docu von users einpflegen

---

## Terminal Installation

* documentation: 
https://gitlab.com/kieselstein-erp/sources/kieselstein-terminal-maui

* download 'windows package'
* execute powershell install script

Preconditions:
* Entwicklermodus?
* Scripting enabled

---

## Terminal next steps

* Zertifikat 
* Test android & iOS installation


---

## Recap Installation

### Prerequisites
* zulu jdk 8 (including fx)
* postgres server installed (localhost:5432), user: postgres, pw: postgres

### Installation
1. download tar
2. unpack tar
3. mkdir data
4. set environment
5. launch server, rest-api, client
6. New: Service (Windows - Linux for main server ready, rest still to do)
7. New landing page -> download client

---

## New:
* More comfortable configuration (-> ports)
* Some user provided experience (Erik, Werner)
* --> special cases 

Ho to have more systems on one machine ==> Changing ports?
* in configuration

---

... or: Docker

## Getting Started demo
### Prerequisites
* zulu jdk 8 (including fx)
* postgres server installed (localhost:5432), user: postgres, pw: postgres

### Installation
1. download tar
2. unpack tar
3. mkdir data
4. set environment
5. launch server, rest-api, client

---

## Work in Progress! - still we need:
* More detailed instructions
* Setup recommendations (directory structure)
* Configuration Best Practices
* Cleanup & More documentation of build processl
* move custom reports from dist --> data
